const { name } = require("ejs");
const express = require("express");
const path = require("path");
const app = express();

app.set("view engine", "ejs");
app.set("views", path.join(__dirname, "views"));
app.use(express.static(path.join(__dirname, "public")));
app.use(express.static(path.join(__dirname, "images")));
// Démarrage du serveur
app.listen(3000, () => {
	console.log("Serveur démarré (http://localhost:3000/) !");
});

app.get("/", (req, res) => {
	// res.send("Bonjour le monde...");
	res.render("index");
});

// GET /
app.get("/Archives", (req, res) => {
	// res.send("Bonjour le monde...");
	res.render("Archives");
});
// GET /
app.get("/Canevas", (req, res) => {
	// res.send("Bonjour le monde...");
	res.render("Canevas");
});
// GET /
app.get("/Chantiers", (req, res) => {
	// res.send("Bonjour le monde...");
	res.render("Chantiers");
});
// GET /
app.get("/Enquete", (req, res) => {
	// res.send("Bonjour le monde...");
	res.render("Enquete");
});
// GET /
app.get("/Temperatures", (req, res) => {
	// res.send("Bonjour le monde...");
	res.render("Temperatures");
});
// GET /
app.get("/Trucs", (req, res) => {
	// res.send("Bonjour le monde...");
	res.render("Trucs");
});
// GET /
app.get("/Vocation", (req, res) => {
	// res.send("Bonjour le monde...");
	res.render("Vocation");
});




